import 'package:codeen_mobile/helper/custom_router.dart';
import 'package:codeen_mobile/helper/string_helper.dart';
import 'package:codeen_mobile/view/main/services/service_details_page.dart';
import 'package:flutter/material.dart';

class ServiceCard extends StatelessWidget {
  final int serviceID;
  final String title;
  final String body;
  final String date;
  final String image;
  final double imageWidth;
  final double textWidth;
  final bool showText;

  const ServiceCard(
      {Key key,
      this.title,
      this.date,
      this.image,
      this.imageWidth = 128,
      this.textWidth = 256,
      this.showText = true,
      this.body, this.serviceID})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => CustomRouter.push(
          context,
          ServiceDetailPage(
            serviceID: this.serviceID,
            title: this.title,
            body: this.body,
            date: this.date,
            image: this.image,
          )),
      child: Card(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: imageWidth,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(image),
                ),
              ),
            ),
            (showText)
                ? Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ConstrainedBox(
                          child: Text(
                            StringHelper.trimText(text: title),
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          constraints: BoxConstraints(maxWidth: textWidth),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Text(date),
                        ),
                      ],
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
