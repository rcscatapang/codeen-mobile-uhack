import 'package:codeen_mobile/helper/string_helper.dart';
import 'package:codeen_mobile/provider/service_provider.dart';
import 'package:codeen_mobile/view/main/services/widget/service_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ServiceTrendingModule extends StatefulWidget {
  @override
  _ServiceTrendingModuleState createState() => _ServiceTrendingModuleState();
}

class _ServiceTrendingModuleState extends State<ServiceTrendingModule> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 8),
          child: Text(
            "Trending",
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Consumer<ServiceProvider>(
          builder: (context, serviceProvider, child) => Container(
            height: 128,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, position) {
                if (serviceProvider.serviceTrendingContainer.length != 0) {
                  return Container(
                    height: 128,
                    child: ServiceCard(
                      serviceID: serviceProvider
                          .serviceTrendingContainer[position].service.id,
                      title: serviceProvider
                          .serviceTrendingContainer[position].service.name,
                      body: serviceProvider.serviceTrendingContainer[position]
                          .service.description,
                      date: StringHelper.convertToReadableDate(serviceProvider
                          .serviceTrendingContainer[position]
                          .service
                          .createdAt),
                      image: serviceProvider
                          .serviceTrendingContainer[position].serviceImage
                          .getImageRoute(),
                      textWidth: MediaQuery.of(context).size.width - 208,
                    ),
                  );
                }

                return Container();
              },
              itemCount: serviceProvider.serviceTrendingContainer.length,
            ),
          ),
        ),
      ],
    );
  }
}
