import 'package:codeen_mobile/controller/user/user_booking_controller.dart';
import 'package:codeen_mobile/view/common/form/schedule_time_picker_dialog.dart';
import 'package:codeen_mobile/view/common/widget/custom_appbar.dart';
import 'package:codeen_mobile/view/common/widget/custom_color.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ServiceDetailPage extends StatefulWidget {
  final int serviceID;
  final String title;
  final String body;
  final String date;
  final String image;

  const ServiceDetailPage(
      {Key key, this.title, this.body, this.date, this.image, this.serviceID})
      : super(key: key);

  @override
  _ServiceDetailPageState createState() => _ServiceDetailPageState();
}

class _ServiceDetailPageState extends State<ServiceDetailPage> {
  DateTime _selectedSchedule;

  String _bookDate;
  String _bookTime;

  @override
  void initState() {
    super.initState();
    _bookDate = "-";
    _bookTime = "-";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.secondaryColor,
      appBar: CustomAppBar(
        titleColor: Colors.white,
        backButtonIconColor: Colors.white,
        appBarBrightness: Brightness.dark,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
            top: -56,
            child: Image.network(
              widget.image,
              fit: BoxFit.fitWidth,
              height: 324,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          Positioned.fill(
            top: 224,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Card(
                    shape: const RoundedRectangleBorder(
                        borderRadius: const BorderRadius.only(
                      topLeft: const Radius.circular(32),
                      topRight: const Radius.circular(32),
                    )),
                    margin: const EdgeInsets.all(0),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 32),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 16, bottom: 4),
                            child: Text(
                              widget.title,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                          Text(widget.date),
                          Padding(
                            padding: const EdgeInsets.only(top: 16),
                            child: Text(
                              widget.body,
                              style: const TextStyle(fontSize: 14),
                            ),
                          ),
                          // Container is placed below to prevent overlapping
                          // with button and request status indicator
                          Container(
                            height: 64,
                          ),
                          _buildBookingDetails("Date", _bookDate, true,
                              changeDetailCallback: () =>
                                  _selectDate(context, null)),
                          _buildBookingDetails("Time", _bookTime, false,
                              changeDetailCallback: () =>
                                  _selectTime(context, null)),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16),
                            child: Align(
                              alignment: Alignment.center,
                              child: _buildBookingSchedulePicker(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildBookingDetails(String detail, String value, bool withDivider,
      {Function changeDetailCallback}) {
    Widget _divider = (withDivider)
        ? const Divider(
            color: Colors.grey,
            height: 6,
          )
        : const SizedBox(
            height: 0,
          );
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Text(
                  detail,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                flex: 1,
              ),
              (value != "-" && changeDetailCallback != null)
                  ? Expanded(
                      flex: 2,
                      child: Row(
                        children: <Widget>[
                          Text(value),
                          const Spacer(),
                          GestureDetector(
                            onTap: changeDetailCallback,
                            child: const Text(
                              "Change",
                              style: const TextStyle(color: Colors.blue),
                            ),
                          )
                        ],
                      ),
                    )
                  : Expanded(
                      flex: 2,
                      child: Text(value),
                    ),
            ],
          ),
        ),
        _divider
      ],
    );
  }

  /// Build and Show Schedule Picker (for Selecting  Date)
  _buildBookingSchedulePicker() {
    if (_bookDate == "-") {
      return Padding(
        padding: const EdgeInsets.only(left: 32, right: 32, bottom: 16),
        child: RaisedButton(
          padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
          color: CustomColor.secondaryColor,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(64))),
          onPressed: () => _selectDate(context, null),
          child: const Text(
            "Select Date",
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      );
    } else if (_bookTime == "-") {
      return Padding(
        padding: const EdgeInsets.only(left: 32, right: 32, bottom: 16),
        child: RaisedButton(
          padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
          color: CustomColor.primaryColor,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(64))),
          onPressed: () => _selectTime(context, null),
          child: const Text(
            "Select Time",
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(left: 32, right: 32, bottom: 16),
        child: RaisedButton(
          padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
          color: CustomColor.secondaryColor,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(64))),
          onPressed: _attemptBooking,
          child: const Text(
            "Book Now",
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      );
    }
  }

  /// Build and Show Select  Time Slot
  Future<Null> _selectTime(
      BuildContext context, List<DateTime> timeSlots) async {
    await showDialog(
        context: context,
        builder: (context) {
          return ScheduleBookingTimePickerDialog(
            timeSlots: [
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 07:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 08:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 09:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 10:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 11:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 12:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 13:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 14:00:00.000'),
              DateTime.parse(
                  DateTime.now().year.toString() + '-01-01 15:00:00.000'),
            ],
            onTimeSelected: _setTime,
          );
        });
  }

  /// Build and Show Select  Date
  /// Triggers when Select Booking Date is Clicked
  Future<Null> _selectDate(
      BuildContext context, List<DateTime> availableDates) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now().add(const Duration(days: 2)),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(
        const Duration(days: 360),
      ),
    );

    if (picked != null && picked != _selectedSchedule) _setDate(picked);
  }

  /// Set Selected  Date
  _setDate(DateTime selectedDateTime) {
    setState(() {
      _selectedSchedule = selectedDateTime;
      var formatter = DateFormat('MMMM d, yyyy');
      _bookDate = formatter.format(selectedDateTime);
    });
  }

  /// Set Selected  Time Slot
  _setTime(DateTime selectedTime) {
    setState(() {
      _selectedSchedule = DateTime(
          _selectedSchedule.year,
          _selectedSchedule.month,
          _selectedSchedule.day,
          selectedTime.hour,
          selectedTime.minute);

      var formatter = DateFormat('hh:mm a');
      String time = formatter.format(selectedTime);

      _bookTime = time;
    });
  }

  /// Attempt Booking
  _attemptBooking() async {
    print("Selected Schedule: " + _selectedSchedule.toString());
    try {
      bool bookingSuccess = await UserBookingController.createUserBooking(
          widget.serviceID, _selectedSchedule);
      if (bookingSuccess) {
        Navigator.of(context).pop();
      }
    } catch (e, stackTrace) {
      print(e);
      print(stackTrace);
    }
  }
}
