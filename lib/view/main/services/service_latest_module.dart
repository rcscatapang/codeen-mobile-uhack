import 'package:codeen_mobile/helper/string_helper.dart';
import 'package:codeen_mobile/provider/service_provider.dart';
import 'package:codeen_mobile/view/main/services/widget/service_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ServiceLatestModule extends StatefulWidget {
  @override
  _ServiceLatestModuleState createState() => _ServiceLatestModuleState();
}

class _ServiceLatestModuleState extends State<ServiceLatestModule> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 4),
          child: Text(
            "Latest",
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Consumer<ServiceProvider>(
          builder: (context, serviceProvider, child) => ListView.builder(
            padding: const EdgeInsets.symmetric(vertical: 0),
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, position) {
              if (serviceProvider.serviceContainers.length != 0) {
                return Container(
                  height: 128,
                  child: ServiceCard(
                    serviceID: serviceProvider
                        .serviceContainers[position].service.id,
                    title: serviceProvider
                        .serviceContainers[position].service.name,
                    body: serviceProvider
                        .serviceContainers[position].service.description,
                    date: StringHelper.convertToReadableDate(serviceProvider
                        .serviceContainers[position].service.createdAt),
                    image: serviceProvider
                        .serviceContainers[position].serviceImage
                        .getImageRoute(),
                    textWidth: MediaQuery.of(context).size.width - 208,
                  ),
                );
              }

              return Container();
            },
            itemCount: serviceProvider.serviceContainers.length,
          ),
        ),
      ],
    );
  }
}
