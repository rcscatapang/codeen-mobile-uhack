import 'package:codeen_mobile/container/user_container.dart';
import 'package:codeen_mobile/model/user/user.dart';
import 'package:codeen_mobile/model/user/user_detail.dart';
import 'package:codeen_mobile/model/user/user_image.dart';
import 'package:codeen_mobile/provider/user_provider.dart';
import 'package:codeen_mobile/view/common/form/customer_profile_entry.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UserProfileModule extends StatefulWidget {
  @override
  _UserProfileModuleState createState() => _UserProfileModuleState();
}

class _UserProfileModuleState extends State<UserProfileModule> {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(builder: (context, userProvider, child) {
      UserContainer userContainer = userProvider.userContainer;
      User user = userContainer.user;
      UserDetail userDetail = userContainer.userDetail;
      UserImage userImage = userContainer.userImage;

      return SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
          child: Column(
            children: <Widget>[
              CustomerProfileEntry(
                  "Vecino Name", userDetail.getFullName(), true),
              CustomerProfileEntry("Address", "126 Ortigas, La Union", true),
              CustomerProfileEntry("Landline", "123 456", true),
              CustomerProfileEntry("Mobile No", "0926 721 8188", true),
              CustomerProfileEntry("Email", user.email, true),
              CustomerProfileEntry("Gender", "Male", true),
              CustomerProfileEntry("Nationality", "Filipino", true),
            ],
          ),
        ),
      );
    });
  }
}
