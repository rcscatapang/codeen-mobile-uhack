import 'package:codeen_mobile/view/main/profile/widget/permit_card.dart';
import 'package:flutter/material.dart';

class UserPermitModule extends StatefulWidget {
  @override
  _UserPermitModuleState createState() => _UserPermitModuleState();
}

class _UserPermitModuleState extends State<UserPermitModule> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        PermitCard(
          title: "Business Permit",
        ),
        PermitCard(
          title: "Zoning Clearance",
        ),
        PermitCard(
          title: "Locational Clearance",
        ),
        PermitCard(
          title: "Electrical Permit",
        ),
      ],
    );
  }
}
