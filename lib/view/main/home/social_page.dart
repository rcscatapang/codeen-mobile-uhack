import 'package:codeen_mobile/controller/post/post_controller.dart';
import 'package:codeen_mobile/helper/string_helper.dart';
import 'package:codeen_mobile/provider/post_provider.dart';
import 'package:codeen_mobile/view/common/container/page_container.dart';
import 'package:codeen_mobile/view/main/post/widget/post_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SocialPage extends StatefulWidget {
  @override
  _SocialPageState createState() => _SocialPageState();
}

class _SocialPageState extends State<SocialPage> {
  PostProvider _postProvider;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _loadInitialPosts());
  }

  @override
  Widget build(BuildContext context) {
    _postProvider = Provider.of<PostProvider>(context);
    return Scaffold(
      body: PageContainer(
        maxWidth: MediaQuery.of(context).size.width,
        child: RefreshIndicator(
          onRefresh: () => _loadInitialPosts(),
          child: ListView.builder(
            itemBuilder: (lvBuilder, position) {
              return Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                child: PostCard(
                    author: _postProvider.postContainers[position].userContainer
                        .userDetail.firstName,
                    title: _postProvider.postContainers[position].post.title,
                    body: _postProvider.postContainers[position].post.body,
                    date: StringHelper.convertToReadableDate(
                        _postProvider.postContainers[position].post.createdAt),
                    image: _postProvider.postContainers[position].postImage
                        .getImageRoute(),
                    userImage: _postProvider
                        .postContainers[position].userContainer.userImage
                        .getImageRoute()),
              );
            },
            itemCount: _postProvider.postContainers.length,
          ),
        ),
      ),
    );
  }

  _loadInitialPosts() async {
    _postProvider.postContainers = await PostController.getInitialPosts();
  }
}
