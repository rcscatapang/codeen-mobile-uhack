import 'package:codeen_mobile/container/service_container.dart';
import 'package:codeen_mobile/controller/service/service_controller.dart';
import 'package:codeen_mobile/provider/service_provider.dart';
import 'package:codeen_mobile/view/common/widget/custom_color.dart';
import 'package:codeen_mobile/view/main/services/service_latest_module.dart';
import 'package:codeen_mobile/view/main/services/service_trending_module.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ActivitiesPage extends StatefulWidget {
  @override
  _ActivitiesPageState createState() => _ActivitiesPageState();
}

class _ActivitiesPageState extends State<ActivitiesPage> {
  ServiceProvider _serviceProvider;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _loadInitialServices());
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _loadInitialTrendingServices());
  }

  @override
  Widget build(BuildContext context) {
    _serviceProvider = Provider.of<ServiceProvider>(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: RefreshIndicator(
          onRefresh: () => _loadInitialServices(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 96,
                color: CustomColor.secondaryColor,
                child: Stack(
                  children: <Widget>[
//                  Positioned(
//                    top: -32,
//                    right: 0,
//                    child: SvgPicture.asset(
//                      'asset/image/pattern/common_pattern.svg',
//                      width: MediaQuery.of(context).size.width,
//                      height: 128,
//                    ),
//                  ),
                    const Positioned(
                      top: 0,
                      bottom: 0,
                      left: 24,
                      child: const Center(
                        child: const Text(
                          "Account",
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: ServiceTrendingModule(),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: ServiceLatestModule(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _loadInitialServices() async {
    List<ServiceContainer> serviceContainers =
        await ServiceController.getInitialServices();

    _serviceProvider.serviceContainers = serviceContainers;
  }

  _loadInitialTrendingServices() async {
    List<ServiceContainer> serviceContainers =
        await ServiceController.getInitialServices();

    _serviceProvider.serviceTrendingContainer = serviceContainers;
  }
}
