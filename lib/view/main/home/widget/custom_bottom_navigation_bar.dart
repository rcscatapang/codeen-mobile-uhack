import 'package:codeen_mobile/provider/page_provider.dart';
import 'package:codeen_mobile/view/common/widget/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  final List<BottomNavigationBarItem> _bottomNavigationBarItems = const [
    const BottomNavigationBarItem(
      activeIcon: const ImageIcon(
        AssetImage("asset/navigation/social.png"),
        color: CustomColor.primaryColor,
        size: 24,
      ),
      icon: const ImageIcon(
        AssetImage("asset/navigation/social_inactive.png"),
        size: 24,
      ),
      title: const Text(
        "Social",
      ),
    ),
    const BottomNavigationBarItem(
      activeIcon: const ImageIcon(
        AssetImage("asset/navigation/news.png"),
        color: CustomColor.primaryColor,
        size: 24,
      ),
      icon: const ImageIcon(
        AssetImage("asset/navigation/news_inactive.png"),
        size: 24,
      ),
      title: const Text(
        "News",
      ),
    ),
    const BottomNavigationBarItem(
      activeIcon: const ImageIcon(
        AssetImage("asset/navigation/activities.png"),
        color: CustomColor.primaryColor,
        size: 24,
      ),
      icon: const ImageIcon(
        AssetImage("asset/navigation/activities_inactive.png"),
        size: 24,
      ),
      title: const Text(
        "Activities",
      ),
    ),
    const BottomNavigationBarItem(
      activeIcon: const ImageIcon(
        AssetImage("asset/navigation/profile.png"),
        color: CustomColor.primaryColor,
        size: 24,
      ),
      icon: const ImageIcon(
        AssetImage("asset/navigation/profile_inactive.png"),
        size: 24,
      ),
      title: const Text(
        "Profile",
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Consumer<PageProvider>(
      builder: (consumerContext, pageProvider, child) => CupertinoTabBar(
        activeColor: CustomColor.primaryColor,
        backgroundColor: Colors.white,
        items: _bottomNavigationBarItems,
        currentIndex: pageProvider.page,
        onTap: (page) {
          pageProvider.page = page;
        },
      ),
    );
  }
}
