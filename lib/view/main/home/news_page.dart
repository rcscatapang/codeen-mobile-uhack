import 'package:codeen_mobile/controller/article/article_controller.dart';
import 'package:codeen_mobile/provider/article_provider.dart';
import 'package:codeen_mobile/view/main/news/article_headline_module.dart';
import 'package:codeen_mobile/view/main/news/article_latest_module.dart';
import 'package:codeen_mobile/view/main/news/article_popular_module.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  ArticleProvider _articleProvider;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _loadInitialArticles());
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _loadInitialPopularArticles());
    WidgetsBinding.instance.addPostFrameCallback((_) => _loadArticleHeadline());
  }

  @override
  Widget build(BuildContext context) {
    _articleProvider = Provider.of<ArticleProvider>(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const ArticleHeadlineModule(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: const ArticlePopularModule(),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: const ArticleLatestModule(),
            ),
          ],
        ),
      ),
    );
  }

  _loadArticleHeadline() async {
    _articleProvider.articleContainer =
        await ArticleController.getArticleHeadline();
  }

  _loadInitialArticles() async {
    _articleProvider.articleContainers =
        await ArticleController.getInitialArticles();
  }

  _loadInitialPopularArticles() async {
    _articleProvider.articlePopularContainers =
        await ArticleController.getInitialPopularArticles();
  }
}
