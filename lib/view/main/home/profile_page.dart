import 'package:codeen_mobile/container/user_container.dart';
import 'package:codeen_mobile/controller/user/user_profile_controller.dart';
import 'package:codeen_mobile/provider/user_provider.dart';
import 'package:codeen_mobile/view/common/widget/custom_color.dart';
import 'package:codeen_mobile/view/main/profile/user_permits_module.dart';
import 'package:codeen_mobile/view/main/profile/user_profile_module.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  PageController _pageViewController;

  UserProvider _userProvider;

  List<Widget> _modules = [UserProfileModule(), UserPermitModule()];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: 2,
      initialIndex: 0,
      vsync: this,
    );

    _pageViewController = PageController(initialPage: 0);
    _tabController.addListener(_onTabChange);

    WidgetsBinding.instance.addPostFrameCallback((_) => _loadUserProfile());
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);

    return (_userProvider.userContainer.user != null) ? Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 96,
            color: CustomColor.secondaryColor,
            child: Stack(
              children: <Widget>[
//                  Positioned(
//                    top: -32,
//                    right: 0,
//                    child: SvgPicture.asset(
//                      'asset/image/pattern/common_pattern.svg',
//                      width: MediaQuery.of(context).size.width,
//                      height: 128,
//                    ),
//                  ),
                const Positioned(
                  top: 0,
                  bottom: 0,
                  left: 24,
                  child: const Center(
                    child: const Text(
                      "Account",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 56,
            child: Material(
              color: Colors.white,
              child: TabBar(
                controller: _tabController,
                isScrollable: false,
                onTap: (position) => _onTabChange,
                tabs: <Widget>[
                  const Tab(
                    child: const Text(
                      "User Profile",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const Tab(
                    child: const Text(
                      "User Permits",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
                labelColor: CustomColor.primaryColor,
                indicatorColor: CustomColor.primaryColor,
                unselectedLabelColor: Colors.black,
                indicatorPadding: const EdgeInsets.symmetric(horizontal: 32),
              ),
            ),
          ),
          Expanded(
            child: PageView(
              scrollDirection: Axis.horizontal,
              physics: const NeverScrollableScrollPhysics(),
              controller: _pageViewController,
              children: _modules,
            ),
          ),
        ],
      ),
    ) : Scaffold ();
  }

  _onTabChange() {
    _pageViewController.animateToPage(_tabController.index,
        duration: Duration(milliseconds: 500), curve: Curves.linearToEaseOut);
  }

  _loadUserProfile() async {
    UserContainer userContainer = await UserProfileController.getUserProfile();
    _userProvider.userContainer = userContainer;
  }
}
