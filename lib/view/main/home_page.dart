import 'package:codeen_mobile/helper/custom_router.dart';
import 'package:codeen_mobile/provider/article_provider.dart';
import 'package:codeen_mobile/provider/page_provider.dart';
import 'package:codeen_mobile/provider/post_provider.dart';
import 'package:codeen_mobile/provider/service_provider.dart';
import 'package:codeen_mobile/provider/user_provider.dart';
import 'package:codeen_mobile/view/main/home/widget/custom_bottom_navigation_bar.dart';
import 'package:codeen_mobile/view/main/post/post_create_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage();

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageProvider _pageProvider;
  PostProvider _postProvider;
  ArticleProvider _articleProvider;
  ServiceProvider _serviceProvider;
  UserProvider _userProvider;

  @override
  void initState() {
    super.initState();
    _pageProvider = PageProvider();
    _postProvider = PostProvider();
    _articleProvider = ArticleProvider();
    _serviceProvider = ServiceProvider();
    _userProvider = UserProvider();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          builder: (_) => _pageProvider,
        ),
        ChangeNotifierProvider(
          builder: (_) => _postProvider,
        ),
        ChangeNotifierProvider(
          builder: (_) => _articleProvider,
        ),
        ChangeNotifierProvider(
          builder: (_) => _serviceProvider,
        ),
        ChangeNotifierProvider(
          builder: (_) => _userProvider,
        ),
      ],
      child: Scaffold(
        backgroundColor: Colors.blueAccent,
        bottomNavigationBar: CustomBottomNavigationBar(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: _navigateToCreatePost,
        ),
        body: Consumer<PageProvider>(
          builder: (consumerContext, pageProvider, child) =>
              pageProvider.pages[pageProvider.page],
        ),
      ),
    );
  }

  _navigateToCreatePost() {
    CustomRouter.push(context, PostCreatePage(postProvider: _postProvider));
  }
}
