import 'package:codeen_mobile/view/common/widget/custom_appbar.dart';
import 'package:codeen_mobile/view/common/widget/custom_color.dart';
import 'package:flutter/material.dart';

class ArticleDetailPage extends StatelessWidget {
  final String title;
  final String body;
  final String date;
  final String image;

  const ArticleDetailPage(
      {Key key, this.title, this.body, this.date, this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.secondaryColor,
      appBar: CustomAppBar(
        backgroundColor: CustomColor.secondaryColor,
        titleColor: Colors.white,
        backButtonIconColor: Colors.white,
        appBarBrightness: Brightness.dark,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            child: Image.network(
              image,
              fit: BoxFit.fitWidth,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          Positioned.fill(
            top: 224,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Card(
                    shape: const RoundedRectangleBorder(
                        borderRadius: const BorderRadius.only(
                      topLeft: const Radius.circular(32),
                      topRight: const Radius.circular(32),
                    )),
                    margin: const EdgeInsets.all(0),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 32),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 16, bottom: 4),
                            child: Text(
                              title,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                          Text(date),
                          Padding(
                            padding: const EdgeInsets.only(top: 16),
                            child: Text(
                              body,
                              style: const TextStyle(fontSize: 14),
                            ),
                          ),
                          // Container is placed below to prevent overlapping
                          // with button and request status indicator
                          Container(
                            height: 80,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
