import 'package:codeen_mobile/helper/custom_router.dart';
import 'package:codeen_mobile/helper/string_helper.dart';
import 'package:codeen_mobile/provider/article_provider.dart';
import 'package:codeen_mobile/view/main/news/page/article_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class ArticleHeadlineModule extends StatelessWidget {
  const ArticleHeadlineModule({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ArticleProvider>(
      builder: (context, articleProvider, child) =>
          (articleProvider.articleContainer != null)
              ? Card(
                  margin: EdgeInsets.all(0),
                  color: Colors.black,
                  borderOnForeground: true,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32),
                      bottomRight: Radius.circular(32),
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () => CustomRouter.push(
                      context,
                      ArticleDetailPage(
                        title: articleProvider.articleContainer.article.title,
                        body: articleProvider.articleContainer.article.body,
                        date: StringHelper.convertToReadableDate(
                            articleProvider.articleContainer.article.createdAt),
                        image: articleProvider.articleContainer.articleImage
                            .getImageRoute(),
                      ),
                    ),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          child: Image.network(
                            articleProvider.articleContainer.articleImage
                                .getImageRoute(),
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                        Positioned(
                          bottom: 32,
                          left: 48,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "TOP HEADLINE",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 10),
                              ),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width *
                                            0.9),
                                child: Text(
                                  StringHelper.trimText(
                                      text: articleProvider
                                          .articleContainer.article.title),
                                  style: const TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Text(
                                StringHelper.convertToReadableDate(
                                    articleProvider
                                        .articleContainer.article.createdAt),
                                style: const TextStyle(color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : Container(),
    );
  }
}
