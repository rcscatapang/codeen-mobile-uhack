import 'package:codeen_mobile/helper/string_helper.dart';
import 'package:codeen_mobile/provider/article_provider.dart';
import 'package:codeen_mobile/view/main/news/widget/article_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ArticleLatestModule extends StatefulWidget {
  const ArticleLatestModule();

  @override
  _ArticleLatestModuleState createState() => _ArticleLatestModuleState();
}

class _ArticleLatestModuleState extends State<ArticleLatestModule> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 4, bottom: 0),
          child: Text(
            "Latest",
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Consumer<ArticleProvider>(
          builder: (context, articleProvider, child) => ListView.builder(
            padding: const EdgeInsets.symmetric(vertical: 4),
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, position) {
              if (articleProvider.articlePopularContainers.length != 0) {
                return Container(
                  height: 128,
                  child: ArticleCard(
                    title: articleProvider
                        .articlePopularContainers[position].article.title,
                    body: articleProvider
                        .articlePopularContainers[position].article.body,
                    date: StringHelper.convertToReadableDate(articleProvider
                        .articlePopularContainers[position].article.createdAt),
                    image: articleProvider
                        .articlePopularContainers[position].articleImage
                        .getImageRoute(),
                    textWidth: MediaQuery.of(context).size.width - 208,
                  ),
                );
              }

              return Container();
            },
            itemCount: articleProvider.articlePopularContainers.length,
          ),
        ),
      ],
    );
  }
}
