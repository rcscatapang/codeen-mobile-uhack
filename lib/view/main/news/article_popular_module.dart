import 'package:codeen_mobile/helper/string_helper.dart';
import 'package:codeen_mobile/provider/article_provider.dart';
import 'package:codeen_mobile/view/main/news/widget/article_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ArticlePopularModule extends StatefulWidget {
  const ArticlePopularModule();

  @override
  _ArticlePopularModuleState createState() => _ArticlePopularModuleState();
}

class _ArticlePopularModuleState extends State<ArticlePopularModule> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 4, bottom: 8),
          child: Text(
            "Most Popular",
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Consumer<ArticleProvider>(
          builder: (context, articleProvider, child) => Container(
            height: 128,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, position) {
                if (articleProvider.articlePopularContainers.length != 0) {
                  return ArticleCard(
                    title: articleProvider
                        .articlePopularContainers[position].article.title,
                    body: articleProvider
                        .articlePopularContainers[position].article.body,
                    date: StringHelper.convertToReadableDate(articleProvider
                        .articlePopularContainers[position].article.createdAt),
                    image: articleProvider
                        .articlePopularContainers[position].articleImage
                        .getImageRoute(),
                    textWidth: MediaQuery.of(context).size.width - 208,
                    showText: false,
                  );
                }

                return Container();
              },
              itemCount: articleProvider.articlePopularContainers.length,
            ),
          ),
        ),
      ],
    );
  }
}
