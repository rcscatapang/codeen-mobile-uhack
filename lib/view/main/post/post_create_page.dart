import 'dart:convert';
import 'dart:io';

import 'package:codeen_mobile/container/post_container.dart';
import 'package:codeen_mobile/controller/post/post_controller.dart';
import 'package:codeen_mobile/provider/post_provider.dart';
import 'package:codeen_mobile/view/common/container/page_container.dart';
import 'package:codeen_mobile/view/common/form/custom_textfield.dart';
import 'package:codeen_mobile/view/common/form/rounded_button.dart';
import 'package:codeen_mobile/view/common/widget/custom_appbar.dart';
import 'package:codeen_mobile/view/common/widget/custom_rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PostCreatePage extends StatefulWidget {
  final PostProvider postProvider;

  const PostCreatePage({Key key, this.postProvider}) : super(key: key);

  @override
  _PostCreatePageState createState() => _PostCreatePageState();
}

class _PostCreatePageState extends State<PostCreatePage> {
  File file;

  TextEditingController _tecTitle = TextEditingController();
  TextEditingController _tecBody = TextEditingController();

  @override
  void dispose() {
    _tecTitle.dispose();
    _tecBody.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: PageContainer(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 32),
              child: Column(
                children: <Widget>[
                  CustomTextField(
                    controller: _tecTitle,
                    placeholder: "What's on your mind?",
                  ),
                  CustomTextField(
                    controller: _tecBody,
                    maxLines: 3,
                    placeholder: "...",
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: RoundedButton(
                      child: Icon(Icons.image),
                      onPressed: _selectImage,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: CustomRoundedButton(
                      text: "Create Post",
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      onPressed: _attemptUpload,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _selectImage() async {
    file = await ImagePicker.pickImage(source: ImageSource.gallery);
  }

  void _attemptUpload() async {
    if (file == null) return;
    String base64Image = base64Encode(file.readAsBytesSync());
    String fileName = file.path.split("/").last;

    String title = _tecTitle.text;
    String body = _tecBody.text;

    PostContainer postContainer = await PostController.createPost(
        title: title, body: body, image: base64Image);

    if (postContainer != null) {
      widget.postProvider.addPostContainerToTop(postContainer);
      Navigator.pop(context);
    }
  }
}
