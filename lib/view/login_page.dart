import 'package:codeen_mobile/helper/custom_router.dart';
import 'package:codeen_mobile/view/common/container/page_container.dart';
import 'package:codeen_mobile/view/common/form/custom_textfield.dart';
import 'package:codeen_mobile/view/common/form/rounded_button.dart';
import 'package:codeen_mobile/view/main/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _tecEmail = TextEditingController();
  TextEditingController _tecPassword = TextEditingController();

  @override
  void dispose() {
    _tecEmail.dispose();
    _tecPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Image.asset(
              'asset/navigation/login_bg.png',
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.fitWidth,
            ),
            Center(
              child: SingleChildScrollView(
                child: PageContainer(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Padding(
                        padding: const EdgeInsets.only(top: 96, bottom: 16),
                        child: const Text(
                          "Welcome!",
                          style: TextStyle(
                            color: Color(0xff333333),
                            fontSize: 32,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      CustomTextField(
                        controller: _tecEmail,
                        placeholder: "Email",
                      ),
                      CustomTextField(
                        controller: _tecPassword,
                        placeholder: "Password",
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 48),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            const Text(
                              "Sign In",
                              style: const TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(
                              width: 32,
                            ),
                            RoundedButton(
                              child: Icon(Icons.chevron_right),
                              onPressed: _attemptLogin,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _attemptLogin() {
    try {
      String email = _tecEmail.text;
      String password = _tecPassword.text;

      // TODO :: Replace with PushAndRemoveStack
      CustomRouter.push(context, const HomePage());
    } catch (e, stackTrace) {
      print(e);
      print(stackTrace);
    }
  }
}
