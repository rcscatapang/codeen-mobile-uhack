import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ScheduleBookingTimePickerDialog extends StatefulWidget {
  final Function onTimeSelected;
  final List<DateTime> timeSlots;

  const ScheduleBookingTimePickerDialog(
      {this.onTimeSelected, @required this.timeSlots});

  @override
  _ScheduleBookingTimePickerDialogState createState() =>
      _ScheduleBookingTimePickerDialogState();
}

class _ScheduleBookingTimePickerDialogState
    extends State<ScheduleBookingTimePickerDialog> {
  DateTime _selectedSchedule;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 0, 16, 8),
                  child: const Text(
                    "Select Time",
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height / 3),
                  child: ListView.builder(
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, position) {
                      return FlatButton(
                        padding: const EdgeInsets.all(0),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Radio(
                              value: widget.timeSlots[position],
                              groupValue: _selectedSchedule,
                              materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                              onChanged: (_) {
                                _selectTimeSchedule(position);
                              },
                            ),
                            Expanded(
                              child: Text(
                                _buildTimeScheduleString(position),
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {
                          _selectTimeSchedule(position);
                        },
                      );
                    },
                    itemCount: widget.timeSlots.length,
                  ),
                ),
              ],
            ),
          ),
          Divider(
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: const Text("Cancel"),
                  onPressed: () => Navigator.pop(context),
                ),
                FlatButton(
                  child: const Text("OK"),
                  onPressed: () {
                    if (widget.onTimeSelected != null &&
                        _selectedSchedule != null) {
                      widget.onTimeSelected(_selectedSchedule);
                      Navigator.pop(context);
                    }
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  _buildTimeScheduleString(int position) {
    var formatter = DateFormat('hh:mm a');

    DateTime _timeScheduleEnd = widget.timeSlots[position];
    _timeScheduleEnd = _timeScheduleEnd.add(const Duration(hours: 1));

    String duration = formatter.format(widget.timeSlots[position]) +
        " - " +
        formatter.format(_timeScheduleEnd);

    return duration;
  }

  _selectTimeSchedule(int position) {
    setState(() {
      _selectedSchedule = widget.timeSlots[position];
    });
  }
}
