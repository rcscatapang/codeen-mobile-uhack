import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final Widget child;
  final double size;
  final VoidCallback onPressed;

  const RoundedButton({Key key, this.child, this.size = 64, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      child: RaisedButton(
        padding: const EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(Radius.circular(32))),
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
