import 'package:flutter/material.dart';

class CustomerProfileEntry extends StatelessWidget {
  final String name;
  final String value;
  final bool withDivider;

  const CustomerProfileEntry(this.name, this.value, this.withDivider);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Text(
                  name,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                child: Text(
                  (value != null) ? value : "",
                  textAlign: TextAlign.right,
                ),
              )
            ],
          ),
        ),
        (withDivider)
            ? const Divider(
          height: 1,
        )
            : const SizedBox(height: 0,),
      ],
    );
  }
}
