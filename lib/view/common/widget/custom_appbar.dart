import 'package:codeen_mobile/view/common/widget/custom_back_button.dart';
import 'package:codeen_mobile/view/common/widget/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final Icon titleIcon;
  final bool backIconVisibility;
  final Color backgroundColor;
  final Color backButtonIconColor;
  final Color titleColor;
  final double elevation;
  final Function backButtonCallback;
  final Brightness appBarBrightness;
  final List<Widget> actions;

  CustomAppBar(
      {this.title = "",
      this.titleColor = Colors.white,
      this.backgroundColor = CustomColor.secondaryColor,
      this.backButtonIconColor = Colors.white,
      this.backIconVisibility = true,
      this.backButtonCallback,
      this.appBarBrightness = Brightness.dark,
      this.titleIcon,
      this.actions,
      this.elevation = 4});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: backgroundColor,
      brightness: appBarBrightness,
      automaticallyImplyLeading: false,
      elevation: elevation,
      leading: (backIconVisibility)
          ? CustomBackButton(
              backButtonIconColor: backButtonIconColor,
              backButtonCallback:
                  (backButtonCallback != null) ? backButtonCallback : null)
          : null,
      actions: actions,
      title: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          (titleIcon != null) ? titleIcon : const Text(""),
          Container(
            width: 8,
          ),
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold, color: titleColor, fontSize: 18),
          ),
        ],
      ),
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => Size(double.infinity, 48);
}
