import 'package:flutter/cupertino.dart';

/// Used for defining and for easier access of custom colors that are widely used within the App
class CustomColor {
  static const Color primaryColor = const Color(0xffcb007e);
  static const Color secondaryColor = const Color(0xff1d174b);
  static const Color secondaryLightColor = const Color(0xff383363);

  static const Color tertiaryColor = const Color(0xffeaeaea);

  static const Color backgroundColor = const Color(0xfff6f6f6);

// static const Color
}