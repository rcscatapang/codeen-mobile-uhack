import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Used for displaying a custom back button
class CustomBackButton extends StatefulWidget {
  final Color backButtonIconColor;
  final VoidCallback backButtonCallback;

  const CustomBackButton(
      {this.backButtonIconColor = Colors.black, this.backButtonCallback});

  @override
  _CustomBackButtonState createState() => _CustomBackButtonState();
}

class _CustomBackButtonState extends State<CustomBackButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 48,
      child: CupertinoButton(
        // borderRadius: const BorderRadius.all(const Radius.circular(0)),
        padding: const EdgeInsets.all(0),
        onPressed: (widget.backButtonCallback != null)
            ? widget.backButtonCallback
            : _navigateToPrevious,
        child: Icon(
          CupertinoIcons.back,
          color: widget.backButtonIconColor,
        ),
      ),
    );
  }

  _navigateToPrevious() {
    Navigator.pop(context);
  }
}
