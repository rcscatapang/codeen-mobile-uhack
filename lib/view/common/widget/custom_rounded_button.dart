import 'package:codeen_mobile/view/common/widget/custom_color.dart';
import 'package:flutter/material.dart';

class CustomRoundedButton extends StatelessWidget {
  final Color backgroundColor;
  final String text;
  final TextStyle textStyle;
  final Widget iconWidget;
  final double borderRadius;
  final EdgeInsets padding;
  final VoidCallback onPressed;

  const CustomRoundedButton({
    Key key,
    this.backgroundColor = CustomColor.primaryColor,
    this.onPressed,
    this.text = "",
    this.textStyle =
    const TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
    this.borderRadius = 64,
    this.padding = const EdgeInsets.fromLTRB(0, 12, 0, 12),
    this.iconWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: padding,
      color: backgroundColor,
      highlightElevation: 4,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
      onPressed: (onPressed != null) ? onPressed : () {},
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          (iconWidget != null)
              ? iconWidget
              : const SizedBox(
            height: 0,
          ),
          Text(text, style: textStyle)
        ],
      ),
    );
  }
}
