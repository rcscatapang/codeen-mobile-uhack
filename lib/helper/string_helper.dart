import 'package:intl/intl.dart';

class StringHelper {
  static String convertToReadableDate(DateTime date) {
    var formatter = DateFormat('MMMM d, yyyy');
    String formatted = formatter.format(date);
    return formatted;
  }

  static String trimText({String text, int maxLength = 48}) {
    if (text.length >= 48) {
      String trimmed = text.substring(0, 44);
      trimmed += "...";

      return trimmed;
    }

    return text;
  }
}
