import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomRouter {
  static push(BuildContext context, dynamic route) async {
    try {
      var page = await _buildPage(route);

      if (Platform.isIOS) {
        Navigator.push(context, CupertinoPageRoute(builder: (_) => page));
      } else {
        Navigator.push(context, MaterialPageRoute(builder: (_) => page));
      }
    } catch (e) {
      print(e.toString());
    }
  }

  static pushAndRemoveStack(BuildContext context, dynamic route) async {
    try {
      var page = await _buildPage(route);

      if (Platform.isIOS) {
        Navigator.pushAndRemoveUntil(
            context,
            CupertinoPageRoute(builder: (_) => page),
            (Route<dynamic> route) => false);
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => page),
            (Route<dynamic> route) => false);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  static Future<Widget> _buildPage(route) async {
    return Future.microtask(() {
      return route;
    });
  }
}
