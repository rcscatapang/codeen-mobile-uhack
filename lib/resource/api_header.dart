class ApiHeader {
  static Map<String, dynamic> getJsonHeader() {
    return {"Content-Type": "application/json", "Accept": "application/json"};
  }

  static Map<String, dynamic> getMultipartHeader() {
    return {"Content-type": "multipart/form-data"};
  }
}
