class ApiRoute {
//   static const String HOST = "http://192.168.43.112:8000";
  static const String HOST = "http://codeen.ap-southeast-1.elasticbeanstalk.com";

  static const String API = HOST + "/api";

  static const String LOGIN = API + "/login";
  static const String SIGNUP = API + "/signup";

  static String CREATE_POST = API + "/posts";
  static String FETCH_INITIAL_POSTS = API + "/posts";

  static String FETCH_NEWER_POSTS(int postID) => API + "/posts/newer/$postID";

  static String FETCH_OLDER_POSTS(int postID) => API + "/posts/older/$postID";

  static String FETCH_ARTICLE_HEADLINE = API + "/article/headline";
  static String FETCH_INITIAL_ARTICLES = API + "/articles";

  static String FETCH_NEWER_ARTICLES(int articleID) =>
      API + "/articles/newer/$articleID";

  static String FETCH_OLDER_ARTICLES(int articleID) =>
      API + "/articles/older/$articleID";

  static String FETCH_INITIAL_POPULAR_ARTICLES = API + "/articles/popular";

  static String FETCH_NEWER_POPULAR_ARTICLES(int articleID) =>
      API + "/articles/popular/newer/$articleID";

  static String FETCH_OLDER_POPULAR_ARTICLES(int articleID) =>
      API + "/articles/popular/older/$articleID";

  static String FETCH_INITIAL_SERVICES = API + "/services";

  static String CREATE_USER_BOOKING = API + "/user/booking";
  static String FETCH_USER_PROFILE = API +"/user/profile";
}
