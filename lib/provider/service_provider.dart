import 'package:codeen_mobile/container/service_container.dart';
import 'package:flutter/foundation.dart';

class ServiceProvider with ChangeNotifier {
  List<ServiceContainer> _serviceContainers;
  List<ServiceContainer> _serviceTrendingContainer;

  ServiceProvider() {
    _serviceContainers = List();
    _serviceTrendingContainer = List();
  }

  addServiceContainerToTop(ServiceContainer serviceContainer) {
    _serviceContainers.insert(0, serviceContainer);
    notifyListeners();
  }

  addServiceContainersToTop(List<ServiceContainer> serviceContainers) {
    for (ServiceContainer serviceContainerToAdd in serviceContainers) {
      bool isExisting = false;

      for (ServiceContainer serviceContainer in _serviceContainers) {
        if (serviceContainer.service.id == serviceContainerToAdd.service.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _serviceContainers.insert(0, serviceContainerToAdd);
      }
    }

    notifyListeners();
  }

  addServiceContainers(List<ServiceContainer> serviceContainers) {
    for (ServiceContainer serviceContainerToAdd in serviceContainers) {
      bool isExisting = false;

      for (ServiceContainer serviceContainer in _serviceContainers) {
        if (serviceContainer.service.id == serviceContainerToAdd.service.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _serviceContainers.add(serviceContainerToAdd);
      }
    }

    notifyListeners();
  }

  List<ServiceContainer> get serviceContainers => _serviceContainers;

  set serviceContainers(List<ServiceContainer> value) {
    _serviceContainers = value;
    notifyListeners();
  }

  List<ServiceContainer> get serviceTrendingContainer =>
      _serviceTrendingContainer;

  set serviceTrendingContainer(List<ServiceContainer> value) {
    _serviceTrendingContainer = value;
    notifyListeners();
  }
}
