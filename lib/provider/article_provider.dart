import 'package:codeen_mobile/container/article_container.dart';
import 'package:flutter/material.dart';

class ArticleProvider with ChangeNotifier {
  ArticleContainer _articleContainer;
  List<ArticleContainer> _articleContainers;
  List<ArticleContainer> _articlePopularContainers;

  ArticleProvider() {
    _articleContainers = List();
    _articlePopularContainers = List();
  }

  addArticleContainerToTop(ArticleContainer articleContainer) {
    _articleContainers.insert(0, articleContainer);
    notifyListeners();
  }

  addArticleContainersToTop(List<ArticleContainer> articleContainers) {
    for (ArticleContainer articleContainerToAdd in articleContainers) {
      bool isExisting = false;

      for (ArticleContainer articleContainer in _articleContainers) {
        if (articleContainer.article.id == articleContainerToAdd.article.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _articleContainers.insert(0, articleContainerToAdd);
      }
    }

    notifyListeners();
  }

  addArticleContainers(List<ArticleContainer> articleContainers) {
    for (ArticleContainer articleContainerToAdd in articleContainers) {
      bool isExisting = false;

      for (ArticleContainer articleContainer in _articleContainers) {
        if (articleContainer.article.id == articleContainerToAdd.article.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _articleContainers.add(articleContainerToAdd);
      }
    }

    notifyListeners();
  }

  addArticlePopularContainerToTop(ArticleContainer articleContainer) {
    _articlePopularContainers.insert(0, articleContainer);
    notifyListeners();
  }

  addArticlePopularContainersToTop(List<ArticleContainer> articleContainers) {
    for (ArticleContainer articleContainerToAdd in articleContainers) {
      bool isExisting = false;

      for (ArticleContainer articleContainer in _articleContainers) {
        if (articleContainer.article.id == articleContainerToAdd.article.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _articlePopularContainers.insert(0, articleContainerToAdd);
      }
    }

    notifyListeners();
  }

  addArticlePopularContainers(List<ArticleContainer> articleContainers) {
    for (ArticleContainer articleContainerToAdd in articleContainers) {
      bool isExisting = false;

      for (ArticleContainer articleContainer in _articleContainers) {
        if (articleContainer.article.id == articleContainerToAdd.article.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _articlePopularContainers.add(articleContainerToAdd);
      }
    }

    notifyListeners();
  }

  List<ArticleContainer> get articleContainers => _articleContainers;

  set articleContainers(List<ArticleContainer> value) {
    _articleContainers = value;
    notifyListeners();
  }

  List<ArticleContainer> get articlePopularContainers =>
      _articlePopularContainers;

  set articlePopularContainers(List<ArticleContainer> value) {
    _articlePopularContainers = value;
    notifyListeners();
  }

  ArticleContainer get articleContainer => _articleContainer;

  set articleContainer(ArticleContainer value) {
    _articleContainer = value;
    notifyListeners();
  }
}
