import 'package:codeen_mobile/container/user_container.dart';
import 'package:flutter/foundation.dart';

class UserProvider with ChangeNotifier {
  UserContainer _userContainer;

  UserProvider() {
    _userContainer = UserContainer();
  }

  UserContainer get userContainer => _userContainer;

  set userContainer(UserContainer value) {
    _userContainer = value;
    notifyListeners();
  }
}
