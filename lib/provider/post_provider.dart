import 'package:codeen_mobile/container/post_container.dart';
import 'package:flutter/foundation.dart';

class PostProvider with ChangeNotifier {
  List<PostContainer> _postContainers;

  List<PostContainer> get postContainers => _postContainers;

  PostProvider() {
    _postContainers = List();
  }

  addPostContainerToTop(PostContainer postContainer) {
    _postContainers.insert(0, postContainer);
    notifyListeners();
  }

  addPostContainersToTop(List<PostContainer> postContainers) {
    for (PostContainer postContainerToAdd in postContainers) {
      bool isExisting = false;

      for (PostContainer postContainer in _postContainers) {
        if (postContainer.post.id == postContainerToAdd.post.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _postContainers.insert(0, postContainerToAdd);
      }
    }

    notifyListeners();
  }

  addPostContainers(List<PostContainer> postContainers) {
    for (PostContainer postContainerToAdd in postContainers) {
      bool isExisting = false;

      for (PostContainer postContainer in _postContainers) {
        if (postContainer.post.id == postContainerToAdd.post.id) {
          isExisting = true;
          break;
        }
      }

      if (!isExisting) {
        _postContainers.add(postContainerToAdd);
      }
    }

    notifyListeners();
  }

  set postContainers(List<PostContainer> value) {
    _postContainers = value;
    notifyListeners();
  }
}
