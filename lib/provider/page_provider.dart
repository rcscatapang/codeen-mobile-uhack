import 'package:codeen_mobile/view/main/home/activities_page.dart';
import 'package:codeen_mobile/view/main/home/news_page.dart';
import 'package:codeen_mobile/view/main/home/profile_page.dart';
import 'package:codeen_mobile/view/main/home/social_page.dart';
import 'package:flutter/cupertino.dart';

class PageProvider with ChangeNotifier {
  List pages = [SocialPage(), NewsPage(), ActivitiesPage(), ProfilePage()];
  int _page;

  /// Default Constructor
  PageProvider() {
    this._page = 0;
  }

  int get page => _page;

  set page(int value) {
    _page = value;
    notifyListeners();
  }
}
