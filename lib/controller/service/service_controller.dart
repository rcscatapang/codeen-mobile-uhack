import 'dart:convert';

import 'package:codeen_mobile/container/service_container.dart';
import 'package:codeen_mobile/model/service/service.dart';
import 'package:codeen_mobile/model/service/service_image.dart';
import 'package:codeen_mobile/model/service/service_type.dart';
import 'package:codeen_mobile/resource/api_route.dart';
import 'package:http/http.dart' as http;

class ServiceController {
  static getInitialServices() async {
    var response = await http.get(ApiRoute.FETCH_INITIAL_SERVICES);
    var responseBody = json.decode(response.body);

    var jsonServices = responseBody['data']['services'] as List;

    List<ServiceContainer> serviceContainers = List();

    for (int i = 0; i < jsonServices.length; i++) {
      var jsonService = jsonServices[i];

      ServiceContainer serviceContainer = ServiceContainer();
      Service service = Service.fromJson(jsonService);
      ServiceType serviceType =
          ServiceType.fromJson(jsonService['service_type']);
      ServiceImage serviceImage =
          ServiceImage.fromJson(jsonService['service_image']);

      serviceContainer.service = service;
      serviceContainer.serviceType = serviceType;
      serviceContainer.serviceImage = serviceImage;

      serviceContainers.add(serviceContainer);
    }

    return serviceContainers;
  }
}
