import 'dart:convert';

import 'package:codeen_mobile/resource/api_route.dart';
import 'package:http/http.dart' as http;

class UserBookingController {
  static createUserBooking(int serviceID, DateTime dateTime) async {
    var params = {
      'reserved_schedule': dateTime.toString(),
      'service_id': serviceID.toString(),
      'user_id': "2",
    };

    var response = await http.post(ApiRoute.CREATE_USER_BOOKING, body: params);
    var responseBody = json.decode(response.body);

//    var jsonUserBooking = responseBody['data']['user_booking'];
//    UserBooking userBooking = UserBooking.fromJson(jsonUserBooking);

    if (response.statusCode == 200) {
      return true;
    }

    return false;
    // return userBooking;
  }
}
