import 'dart:convert';

import 'package:codeen_mobile/container/user_container.dart';
import 'package:codeen_mobile/model/user/user.dart';
import 'package:codeen_mobile/model/user/user_detail.dart';
import 'package:codeen_mobile/model/user/user_image.dart';
import 'package:codeen_mobile/resource/api_route.dart';
import 'package:http/http.dart' as http;

class UserProfileController {
  static getUserProfile() async {
    var response = await http.get(ApiRoute.FETCH_USER_PROFILE);
    var responseBody = json.decode(response.body);

    var jsonUser = responseBody['data']['user'];
    var jsonUserDetail = responseBody['data']['user']['user_detail'];
    var jsonUserImage = responseBody['data']['user']['user_image'];

    User user = User.fromJson(jsonUser);
    UserDetail userDetail = UserDetail.fromJson(jsonUserDetail);
    UserImage userImage = UserImage.fromJson(jsonUserImage);

    UserContainer userContainer = new UserContainer();
    userContainer.user = user;
    userContainer.userDetail = userDetail;
    userContainer.userImage = userImage;

    return userContainer;
  }
}
