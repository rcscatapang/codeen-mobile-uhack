import 'dart:convert';

import 'package:codeen_mobile/container/post_container.dart';
import 'package:codeen_mobile/container/user_container.dart';
import 'package:codeen_mobile/model/post/post.dart';
import 'package:codeen_mobile/model/post/post_image.dart';
import 'package:codeen_mobile/model/user/user.dart';
import 'package:codeen_mobile/model/user/user_detail.dart';
import 'package:codeen_mobile/model/user/user_image.dart';
import 'package:codeen_mobile/resource/api_route.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class PostController {
  static createPost(
      {@required String title,
      @required String body,
      @required String image}) async {
    var params = {"user_id": "2", "title": title, "body": body, "image": image};

    var response = await http.post(ApiRoute.CREATE_POST, body: params);
    var responseBody = json.decode(response.body);

    PostContainer postContainer = PostContainer();

    UserContainer userContainer = UserContainer();
    userContainer.user = User.fromJson(responseBody['data']['post']['user']);
    userContainer.userDetail = UserDetail.fromJson(
        responseBody['data']['post']['user']['user_detail']);
    userContainer.userImage =
        UserImage.fromJson(responseBody['data']['post']['user']['user_image']);

    postContainer.userContainer = userContainer;
    postContainer.post = Post.fromJson(responseBody['data']['post']);
    postContainer.postImage =
        PostImage.fromJson(responseBody['data']['post']['post_image']);

    print(postContainer.postImage.getImageRoute());
    return postContainer;
  }

  static getInitialPosts() async {
    var response = await http.get(ApiRoute.FETCH_INITIAL_POSTS);
    var responseBody = json.decode(response.body);

    var jsonPosts = responseBody['data']['posts'] as List;

    List<PostContainer> postContainers = List();
    for (int i = 0; i < jsonPosts.length; i++) {
      var jsonPost = jsonPosts[i];

      PostContainer postContainer = PostContainer();

      UserContainer userContainer = UserContainer();
      userContainer.user = User.fromJson(jsonPost['user']);
      userContainer.userDetail =
          UserDetail.fromJson(jsonPost['user']['user_detail']);
      userContainer.userImage =
          UserImage.fromJson(jsonPost['user']['user_image']);

      postContainer.userContainer = userContainer;
      postContainer.post = Post.fromJson(jsonPost);
      postContainer.postImage = PostImage.fromJson(jsonPost['post_image']);

      postContainers.add(postContainer);
    }

    return postContainers;
  }

  static getNewerPosts(int postID) async {
    var response = await http.get(ApiRoute.FETCH_NEWER_POSTS(postID));
    var responseBody = json.decode(response.body);

    var jsonPosts = responseBody['data']['posts'] as List;

    List<PostContainer> postContainers = List();
    for (int i = 0; i < jsonPosts.length; i++) {
      var jsonPost = jsonPosts[i];

      PostContainer postContainer = PostContainer();
      postContainer.post = Post.fromJson(jsonPost);
      postContainer.postImage = PostImage.fromJson(jsonPost['post_image']);
      postContainers.add(postContainer);
    }

    return postContainers;
  }

  static getOlderPosts(int postID) async {
    var response = await http.get(ApiRoute.FETCH_OLDER_POSTS(postID));
    var responseBody = json.decode(response.body);

    var jsonPosts = responseBody['data']['posts'] as List;

    List<PostContainer> postContainers = List();
    for (int i = 0; i < jsonPosts.length; i++) {
      var jsonPost = jsonPosts[i];

      PostContainer postContainer = PostContainer();
      postContainer.post = Post.fromJson(jsonPost);
      postContainer.postImage = PostImage.fromJson(jsonPost['post_image']);
      postContainers.add(postContainer);
    }

    return postContainers;
  }
}
