import 'dart:convert';

import 'package:codeen_mobile/container/article_container.dart';
import 'package:codeen_mobile/model/article/article.dart';
import 'package:codeen_mobile/model/article/article_image.dart';
import 'package:codeen_mobile/resource/api_route.dart';
import 'package:http/http.dart' as http;

class ArticleController {
  static getArticleHeadline() async {
    var response = await http.get(ApiRoute.FETCH_ARTICLE_HEADLINE);
    var responseBody = json.decode(response.body);

    var jsonArticle = responseBody['data']['article'];
    ArticleContainer articleContainer = ArticleContainer();
    articleContainer.article = Article.fromJson(jsonArticle);
    articleContainer.articleImage =
        ArticleImage.fromJson(jsonArticle['article_image']);

    return articleContainer;
  }

  static getInitialArticles() async {
    var response = await http.get(ApiRoute.FETCH_INITIAL_ARTICLES);
    var responseBody = json.decode(response.body);

    var jsonArticles = responseBody['data']['articles'] as List;

    List<ArticleContainer> articleContainers = List();
    for (int i = 0; i < jsonArticles.length; i++) {
      var jsonArticle = jsonArticles[i];

      ArticleContainer articleContainer = ArticleContainer();
      articleContainer.article = Article.fromJson(jsonArticle);
      articleContainer.articleImage =
          ArticleImage.fromJson(jsonArticle['article_image']);

      articleContainers.add(articleContainer);
    }

    return articleContainers;
  }

  static getInitialPopularArticles() async {
    var response = await http.get(ApiRoute.FETCH_INITIAL_POPULAR_ARTICLES);
    var responseBody = json.decode(response.body);

    var jsonArticles = responseBody['data']['articles'] as List;

    List<ArticleContainer> articleContainers = List();
    for (int i = 0; i < jsonArticles.length; i++) {
      var jsonArticle = jsonArticles[i];

      ArticleContainer articleContainer = ArticleContainer();
      articleContainer.article = Article.fromJson(jsonArticle);
      articleContainer.articleImage =
          ArticleImage.fromJson(jsonArticle['article_image']);

      articleContainers.add(articleContainer);
    }

    return articleContainers;
  }

  static getNewerArticles(articleID) async {
    var response = await http.get(ApiRoute.FETCH_NEWER_ARTICLES(articleID));
    var responseBody = json.decode(response.body);

    var jsonArticles = responseBody['data']['articles'] as List;

    List<ArticleContainer> articleContainers = List();
    for (int i = 0; i < jsonArticles.length; i++) {
      var jsonArticle = jsonArticles[i];

      ArticleContainer articleContainer = ArticleContainer();
      articleContainer.article = Article.fromJson(jsonArticle);
      articleContainer.articleImage =
          ArticleImage.fromJson(jsonArticle['article_image']);

      articleContainers.add(articleContainer);
    }

    return articleContainers;
  }

  static getNewerPopularArticles(articleID) async {
    var response =
        await http.get(ApiRoute.FETCH_NEWER_POPULAR_ARTICLES(articleID));
    var responseBody = json.decode(response.body);

    var jsonArticles = responseBody['data']['articles'] as List;

    List<ArticleContainer> articleContainers = List();
    for (int i = 0; i < jsonArticles.length; i++) {
      var jsonArticle = jsonArticles[i];

      ArticleContainer articleContainer = ArticleContainer();
      articleContainer.article = Article.fromJson(jsonArticle);
      articleContainer.articleImage =
          ArticleImage.fromJson(jsonArticle['article_image']);

      articleContainers.add(articleContainer);
    }

    return articleContainers;
  }

  static getOlderArticles(articleID) async {
    var response = await http.get(ApiRoute.FETCH_OLDER_ARTICLES(articleID));
    var responseBody = json.decode(response.body);

    var jsonArticles = responseBody['data']['articles'] as List;

    List<ArticleContainer> articleContainers = List();
    for (int i = 0; i < jsonArticles.length; i++) {
      var jsonArticle = jsonArticles[i];

      ArticleContainer articleContainer = ArticleContainer();
      articleContainer.article = Article.fromJson(jsonArticle);
      articleContainer.articleImage =
          ArticleImage.fromJson(jsonArticle['article_image']);

      articleContainers.add(articleContainer);
    }

    return articleContainers;
  }

  static getOlderPopularArticles(articleID) async {
    var response =
        await http.get(ApiRoute.FETCH_OLDER_POPULAR_ARTICLES(articleID));
    var responseBody = json.decode(response.body);

    var jsonArticles = responseBody['data']['articles'] as List;

    List<ArticleContainer> articleContainers = List();
    for (int i = 0; i < jsonArticles.length; i++) {
      var jsonArticle = jsonArticles[i];

      ArticleContainer articleContainer = ArticleContainer();
      articleContainer.article = Article.fromJson(jsonArticle);
      articleContainer.articleImage =
          ArticleImage.fromJson(jsonArticle['article_image']);

      articleContainers.add(articleContainer);
    }

    return articleContainers;
  }
}
