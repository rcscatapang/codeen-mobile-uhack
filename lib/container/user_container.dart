import 'package:codeen_mobile/model/user/user.dart';
import 'package:codeen_mobile/model/user/user_detail.dart';
import 'package:codeen_mobile/model/user/user_image.dart';

class UserContainer {
  User user;
  UserDetail userDetail;
  UserImage userImage;
}
