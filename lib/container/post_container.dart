import 'package:codeen_mobile/container/user_container.dart';
import 'package:codeen_mobile/model/post/post.dart';
import 'package:codeen_mobile/model/post/post_image.dart';

class PostContainer {
  UserContainer userContainer;
  Post post;
  PostImage postImage;
}
