import 'package:codeen_mobile/model/service/service.dart';
import 'package:codeen_mobile/model/service/service_image.dart';
import 'package:codeen_mobile/model/service/service_type.dart';

class ServiceContainer {
  Service service;
  ServiceType serviceType;
  ServiceImage serviceImage;
}
