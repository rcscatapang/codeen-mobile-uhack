import 'package:codeen_mobile/model/article/article.dart';
import 'package:codeen_mobile/model/article/article_image.dart';

class ArticleContainer {
  Article article;
  ArticleImage articleImage;
}
