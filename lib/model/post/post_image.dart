import 'package:codeen_mobile/resource/api_route.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post_image.g.dart';

@JsonSerializable()
class PostImage {
  @JsonKey(name: 'post_id')
  int postID;
  @JsonKey(name: 'image_link')
  String imageLink;
  @JsonKey(name: 'created_at')
  DateTime createdAt;
  @JsonKey(name: 'updated_at')
  DateTime updatedAt;

  getImageRoute() {
    if (imageLink != null) return ApiRoute.HOST + imageLink;

    return null;
  }

  PostImage();

  factory PostImage.fromJson(Map<String, dynamic> json) =>
      _$PostImageFromJson(json);

  Map<String, dynamic> toJson() => _$PostImageToJson(this);
}
