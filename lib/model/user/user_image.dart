import 'package:codeen_mobile/resource/api_route.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user_image.g.dart';

@JsonSerializable()
class UserImage{
  int id;
  @JsonKey(name: 'user_id')
  int userID;
  @JsonKey(name: 'image_link')
  String imageLink;
  @JsonKey(name: 'created_at')
  DateTime createdAt;
  @JsonKey(name: 'updated_at')
  DateTime updatedAt;

  getImageRoute() {
    if (imageLink != null) return ApiRoute.HOST + imageLink;

    return null;
  }

  UserImage();
  factory UserImage.fromJson(Map<String, dynamic> json) => _$UserImageFromJson(json);
  Map<String, dynamic> toJson() => _$UserImageToJson(this);
}