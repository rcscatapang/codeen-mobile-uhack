// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_permit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserPermit _$UserPermitFromJson(Map<String, dynamic> json) {
  return UserPermit()
    ..id = json['id'] as int
    ..userID = json['user_id'] as int
    ..permitTypeID = json['permit_type_id'] as int
    ..status = json['status'] as String;
}

Map<String, dynamic> _$UserPermitToJson(UserPermit instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userID,
      'permit_type_id': instance.permitTypeID,
      'status': instance.status,
    };
