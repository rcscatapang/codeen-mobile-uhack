import 'package:json_annotation/json_annotation.dart';
part 'user_booking.g.dart';

@JsonSerializable()
class UserBooking {
  int id;
  @JsonKey(name: 'user_id')
  int userID;
  @JsonKey(name: 'service_id')
  int serviceID;
  @JsonKey(name: 'reserved_schedule')
  DateTime reservedSchedule;

  UserBooking();
  factory UserBooking.fromJson(Map<String, dynamic> json) => _$UserBookingFromJson(json);
  Map<String, dynamic> toJson() => _$UserBookingToJson(this);
}
