import 'package:json_annotation/json_annotation.dart';
part 'user_type.g.dart';

@JsonSerializable()
class UserType {
  int id;
  String name;
}
