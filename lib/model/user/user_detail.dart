import 'package:json_annotation/json_annotation.dart';
part 'user_detail.g.dart';

@JsonSerializable()
class UserDetail {
  int id;
  @JsonKey(name: 'user_id')
  int userID;
  @JsonKey(name: 'first_name')
  String firstName;
  @JsonKey(name: 'last_name')
  String lastName;

  getFullName(){
    return this.lastName + ", " + this.firstName;
  }

  UserDetail();
  factory UserDetail.fromJson(Map<String, dynamic> json) => _$UserDetailFromJson(json);
  Map<String, dynamic> toJson() => _$UserDetailToJson(this);
}