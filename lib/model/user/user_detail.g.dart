// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserDetail _$UserDetailFromJson(Map<String, dynamic> json) {
  return UserDetail()
    ..id = json['id'] as int
    ..userID = json['user_id'] as int
    ..firstName = json['first_name'] as String
    ..lastName = json['last_name'] as String;
}

Map<String, dynamic> _$UserDetailToJson(UserDetail instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userID,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
    };
