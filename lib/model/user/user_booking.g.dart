// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_booking.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserBooking _$UserBookingFromJson(Map<String, dynamic> json) {
  return UserBooking()
    ..id = json['id'] as int
    ..userID = json['user_id'] as int
    ..serviceID = json['service_id'] as int
    ..reservedSchedule = json['reserved_schedule'] == null
        ? null
        : DateTime.parse(json['reserved_schedule'] as String);
}

Map<String, dynamic> _$UserBookingToJson(UserBooking instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userID,
      'service_id': instance.serviceID,
      'reserved_schedule': instance.reservedSchedule?.toIso8601String(),
    };
