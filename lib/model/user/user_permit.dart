import 'package:json_annotation/json_annotation.dart';
part 'user_permit.g.dart';

@JsonSerializable()
class UserPermit {
  int id;
  @JsonKey(name: 'user_id')
  int userID;
  @JsonKey(name: 'permit_type_id')
  int permitTypeID;
  String status;

  UserPermit();
  factory UserPermit.fromJson(Map<String, dynamic> json) => _$UserPermitFromJson(json);
  Map<String, dynamic> toJson() => _$UserPermitToJson(this);
}
