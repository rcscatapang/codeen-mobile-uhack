import 'package:codeen_mobile/resource/api_route.dart';
import 'package:json_annotation/json_annotation.dart';
part 'article_image.g.dart';

@JsonSerializable()
class ArticleImage {
  int id;
  @JsonKey(name: 'article_id')
  int articleID;
  @JsonKey(name: 'image_link')
  String imageLink;

  getImageRoute() {
    if (imageLink != null) return ApiRoute.HOST + imageLink;

    return null;
  }

  ArticleImage();
  factory ArticleImage.fromJson(Map<String, dynamic> json) => _$ArticleImageFromJson(json);
  Map<String, dynamic> toJson() => _$ArticleImageToJson(this);
}