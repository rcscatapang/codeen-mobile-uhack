// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArticleImage _$ArticleImageFromJson(Map<String, dynamic> json) {
  return ArticleImage()
    ..id = json['id'] as int
    ..articleID = json['article_id'] as int
    ..imageLink = json['image_link'] as String;
}

Map<String, dynamic> _$ArticleImageToJson(ArticleImage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'article_id': instance.articleID,
      'image_link': instance.imageLink,
    };
