import 'package:json_annotation/json_annotation.dart';
part 'city_development.g.dart';

@JsonSerializable()
class CityDevelopment {
  int id;
  @JsonKey(name: 'city_id')
  int cityID;
  String name;
  String status;

  CityDevelopment();
  factory CityDevelopment.fromJson(Map<String, dynamic> json) => _$CityDevelopmentFromJson(json);
  Map<String, dynamic> toJson() => _$CityDevelopmentToJson(this);
}
