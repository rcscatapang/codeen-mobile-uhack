// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_development.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CityDevelopment _$CityDevelopmentFromJson(Map<String, dynamic> json) {
  return CityDevelopment()
    ..id = json['id'] as int
    ..cityID = json['city_id'] as int
    ..name = json['name'] as String
    ..status = json['status'] as String;
}

Map<String, dynamic> _$CityDevelopmentToJson(CityDevelopment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'city_id': instance.cityID,
      'name': instance.name,
      'status': instance.status,
    };
