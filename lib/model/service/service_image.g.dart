// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceImage _$ServiceImageFromJson(Map<String, dynamic> json) {
  return ServiceImage()
    ..id = json['id'] as int
    ..serviceID = json['service_id'] as int
    ..imageLink = json['image_link'] as String;
}

Map<String, dynamic> _$ServiceImageToJson(ServiceImage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'service_id': instance.serviceID,
      'image_link': instance.imageLink,
    };
