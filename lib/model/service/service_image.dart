import 'package:codeen_mobile/resource/api_route.dart';
import 'package:json_annotation/json_annotation.dart';
part 'service_image.g.dart';

@JsonSerializable()
class ServiceImage {
  int id;
  @JsonKey(name: 'service_id')
  int serviceID;
  @JsonKey(name: 'image_link')
  String imageLink;

  getImageRoute() {
    if (imageLink != null) return ApiRoute.HOST + imageLink;

    return null;
  }

  ServiceImage();
  factory ServiceImage.fromJson(Map<String, dynamic> json) => _$ServiceImageFromJson(json);
  Map<String, dynamic> toJson() => _$ServiceImageToJson(this);
}
